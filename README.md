# Installation

* Enable module `menu_lock`

# Usage

* Go to `admin/people/permissions`: there should be a permission
  `Move menu links across menus`
* Go to `admin/structure/menu/manage/%menu` (e.g.
  `admin/structure/menu/manage/main`)
* Add or edit a menu link
  * Users *with* the permission `Move menu links across menus` will see
    options from *all* menus under the dropdown `Parent link`, and as a
    result will be able to move the menu link to another menu (default
    behaviour of Drupal core)
  * Users *without* this permission will only see options from the
    *current* menu (e.g. the main menu), and as a result will not be able
    to move the menu link to another menu

# Support

This module will be deprecated once supported by Drupal core:
* https://www.drupal.org/project/drupal/issues/3110371
* https://www.drupal.org/node/3250632
