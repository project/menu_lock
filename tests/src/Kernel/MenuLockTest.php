<?php

namespace Drupal\Tests\menu_lock\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test permissions for module menu_lock.
 *
 * @group menu_lock
 */
class MenuLockTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * HTTP kernel service.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * {@inheritdoc}
   *
   * @var string[]
   */
  protected static $modules = [
    'link',
    'menu_lock',
    'menu_link_content',
    'menu_ui',
    'system',
    'user',
  ];

  /**
   * Dummy menu link.
   *
   * @var \Drupal\menu_link_content\Entity\MenuLinkContent
   */
  protected $link;

  /**
   * Site manager user account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $siteManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->httpKernel = $this->container->get('http_kernel');
    $this->installEntitySchema('menu_link_content');

    // Create 2 menus: footer, main.
    $menus = [
      'footer' => 'Footer',
      'main' => 'Main',
    ];
    foreach ($menus as $id => $label) {
      Menu::create(['id' => $id, 'label' => $label])->save();
    }

    // Create menu item in main menu.
    $this->link = MenuLinkContent::create([
      'title' => 'Test menu item',
      'link' => ['uri' => 'route:<nolink>'],
      'menu_name' => 'main',
    ]);
    $this->link->save();
  }

  /**
   * Test user *allowed* to move menu links to other menus.
   */
  public function testWithPerm(): void {
    $this->siteManager = $this->setUpCurrentUser([], [
      'administer menu',
      'move menu links across menus',
    ]);

    $uris = [
      // Test adding a new menu link.
      'admin/structure/menu/manage/main/add',
      // Test editing an existing menu link.
      '/admin/structure/menu/item/' . $this->link->id() . '/edit',
    ];

    foreach ($uris as $uri) {
      // Load menu link URI.
      $request = Request::create($uri);
      $response = $this->httpKernel->handle($request)->prepare($request);
      $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
      $this->setRawContent($response->getContent());

      // Both footer and main menus show up under field 'Parent link'.
      $this->assertSelectOption('menu_parent', 'main:');
      $this->assertSelectOption('menu_parent', 'footer:');
    }
  }

  /**
   * Test user *not* allowed to move menu links to other menus.
   */
  public function testWithoutPerm(): void {
    $this->siteManager = $this->setUpCurrentUser([], [
      'administer menu',
    ]);

    $uris = [
      // Test adding a new menu link.
      'admin/structure/menu/manage/main/add',
      // Test editing an existing menu link.
      '/admin/structure/menu/item/' . $this->link->id() . '/edit',
    ];

    foreach ($uris as $uri) {
      // Load menu link URI.
      $request = Request::create($uri);
      $response = $this->httpKernel->handle($request)->prepare($request);
      $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
      $this->setRawContent($response->getContent());

      // Both footer and main menus show up under field 'Parent link'.
      $this->assertSelectOption('menu_parent', 'main:');
      $this->assertNoSelectOption('menu_parent', 'footer:');
    }
  }

  /**
   * Assert that a select field *does* contain a given option.
   *
   * @param string $select_name
   *   Select field name.
   * @param string $option_value
   *   Option value.
   *
   * @see Drupal\Tests\WebAssert::assertOptionByText()
   */
  protected function assertSelectOption($select_name, $option_value): void {
    $message = strtr('Select field %select contains an option %option.', [
      '%select' => $select_name,
      '%option' => $option_value,
    ]);
    $options = $this->cssSelect('select[name="' . $select_name . '"] > option');
    $this->assertNotEmpty($options);
    foreach ($options as $option) {
      if ($option->attributes()->value == $option_value) {
        return;
      }
    }
    $this->fail($message);
  }

  /**
   * Assert that a select field does *not* contain a given option.
   *
   * @param string $select_name
   *   Select field name.
   * @param string $option_value
   *   Option value.
   *
   * @see Drupal\Tests\WebAssert::assertOptionByText()
   */
  protected function assertNoSelectOption($select_name, $option_value): void {
    $message = strtr('Select field %select does not contain an option %option.', [
      '%select' => $select_name,
      '%option' => $option_value,
    ]);
    $options = $this->cssSelect('select[name="' . $select_name . '"] > option');
    $this->assertNotEmpty($options);
    foreach ($options as $option) {
      if ($option->attributes()->value == $option_value) {
        $this->fail($message);
      }
    }
  }

}
